package com.mountains.mountainfinderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MountainFinderServiceApplication  {

    public static void main(String[] args) {
        SpringApplication.run(MountainFinderServiceApplication.class, args);
    }

}